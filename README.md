Firewalld
=========

A role to set up firewalld

Requirements
------------

None


Role Variables
--------------

A single list that sets up the services for the firewall.

Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: ansible-firewalld,
             firewall_services: [http,https] }

License
-------

BSD

Author Information
------------------

John Hooks
